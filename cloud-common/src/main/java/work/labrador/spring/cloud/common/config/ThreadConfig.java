package work.labrador.spring.cloud.common.config;

import io.netty.util.concurrent.DefaultThreadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Jon
 * @desc todo
 * @date 2021年08月26日 21:53
 */
@Configuration
public class ThreadConfig {

    @Bean
    public ThreadPoolExecutor executor(){
        int processors = 4;
        return new ThreadPoolExecutor(processors,processors*2,60L,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(12000),new DefaultThreadFactory("线程池"),
                new ThreadPoolExecutor.AbortPolicy());
    }
}
