package work.labrador.spring.cloud.common.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
public class RedisCache implements Cache {

    RedisTemplate redisTemplate;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final String id;

    public RedisCache(String id) {
        if (id == null) {
            throw new IllegalArgumentException("缓存实例需要一个id!");
        } else {
            log.info("开启Redis缓存: id = {}", id);
            this.id = id;
        }
    }

    @Override
    public String getId() {
        return this.id.toString();
    }

    @Override
    public int getSize() {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            Long size = redisTemplate.opsForHash().size(this.id.toString());
            log.info("Redis缓存大小: id = {}, size = {}", id, size);
            return size.intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void putObject(final Object key, final Object value) {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            log.info("设置Redis缓存: id = {}, key = {}, value = {}", id, key, value);
            redisTemplate.opsForHash().put(this.id.toString(), key.toString(), value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getObject(final Object key) {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            Object hashVal = redisTemplate.opsForHash().get(this.id.toString(), key.toString());
            log.info("获取Redis缓存: id = {}, key = {}, hashVal = {}", id, key, hashVal);
            return hashVal;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object removeObject(final Object key) {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            redisTemplate.opsForHash().delete(this.id.toString(), key.toString());
            log.info("移除Redis缓存: id = {}, key = {}", id, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void clear() {
        try {
            if (redisTemplate == null) {
                redisTemplate = getRedisTemplate();
            }
            redisTemplate.delete(this.id.toString());
            log.info("清空Redis缓存: id = {}", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }

    private RedisTemplate getRedisTemplate() {
        if (redisTemplate == null) {
            redisTemplate = (RedisTemplate<String, Object>) ContainerUtils.getBean("redisTemplate");
            return redisTemplate;
        }
        return redisTemplate;
    }
}
