package work.labrador.spring.cloud.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> implements Serializable {

    /**
     * 返回值
     */
    @ApiModelProperty(value = "返回码",dataType = "Integer")
    private int code;
    /**
     * 消息
     */
    @ApiModelProperty(value = "消息提示",dataType = "String")
    private String msg;
    /**
     * 数据泛型
     */
    @ApiModelProperty(value = "泛型数据")
    private T data;

    public static <T> Result<T> ok(){
        return new Result<>(200, "处理成功", null);
    }

    public static <T> Result<T> ok(T data){
        return new Result<>(200, "处理成功", data);
    }

    public static <T> Result<T> ok(int code,String msg){
        return new Result<>(code, msg, null);
    }

    public static <T> Result<T> ok(int code,String msg,T data){
        return new Result<>(code, msg, data);
    }

    public static <T> Result<T> error(){
        return new Result<>(201, "处理失败", null);
    }

    public static <T> Result<T> error(String msg){
        return new Result<>(201, msg, null);
    }

    public static <T> Result<T> error(int code,String msg){
        return new Result<>(code, msg, null);
    }
}
