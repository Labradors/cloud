package work.labrador.spring.cloud.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import work.labrador.spring.cloud.common.Result;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;

/**
 * Name:GraceExceptionHandler
 * Description: 统一的异常处理器，服务中抛出的所有异常都经过这里处理后再抛给调用的前端
 * Author:Labradors
 * Time: 2017/7/7 16:19
 */
@ControllerAdvice
@Slf4j
public class GraceExceptionHandler {

    /**
     * 处理业务发现问题主动抛出的异常
     *
     * @param request
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseBody
    public ResponseEntity<Result> baseErrorHandler(HttpServletRequest request, AccessDeniedException e) throws Exception {
        //把错误输出到日志
        log.error("RESTfulException Handler---Host: {} invokes url: {} ERROR: {}", request.getRemoteHost(), request.getRequestURL(), e.getMessage());
        return ResponseEntity.ok(Result.error("暂无权限..."));
    }

    /**
     * 系统抛出的没有处理过的异常
     *
     * @param request
     * @param e
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseEntity<Result> defaultErrorHandler(HttpServletRequest request, Exception e){
        //把错误输出到日志
        log.error("DefaultException Handler---Host: {} invokes url: {} ERROR: {}", request.getRemoteHost(), request.getRequestURL(), e.getMessage());
        return ResponseEntity.ok(Result.error(e.getMessage()));
    }
}
