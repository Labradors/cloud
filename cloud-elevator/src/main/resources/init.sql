drop table if exists elevator_company;
CREATE TABLE `elevator_company`  (
                             `id` BIGINT(20) NOT NULL COMMENT '主键ID',
                             `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL UNIQUE COMMENT '公司名称',
                             `address` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '公司地址',
                             `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL  UNIQUE COMMENT '公司邮箱',
                             `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL UNIQUE COMMENT '手机号',
                             `province_id` BIGINT(20)  NOT NULL COMMENT '省份ID',
                             `city_id` BIGINT(20)  NOT NULL COMMENT '市ID',
                             `country_id` BIGINT(20)  NOT NULL COMMENT '区ID',
                             `type` INT(1) NOT NULL DEFAULT 0 COMMENT '公司类型',
                             `create_time` BIGINT(10) NOT NULL COMMENT '创建时间',
                             `update_time` BIGINT(10) NOT NULL COMMENT '修改时间',
                             `enable` BOOLEAN NOT NULL DEFAULT TRUE COMMENT '是否可用',
                             PRIMARY KEY (`id`) USING BTREE

) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

drop table if exists elevator_user_company;
CREATE TABLE `elevator_user_company`  (
                             `id` BIGINT(20) NOT NULL PRIMARY KEY COMMENT '主键ID',
                             `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '公司名称',
                             `company_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '公司地址',
                             `create_time` BIGINT(10) NOT NULL COMMENT '创建时间',
                             `update_time` BIGINT(10) NOT NULL COMMENT '修改时间',
                             `enable` BOOLEAN NOT NULL DEFAULT TRUE COMMENT '是否可用',
                              UNIQUE uc (`user_id`,`company_id`)

) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;