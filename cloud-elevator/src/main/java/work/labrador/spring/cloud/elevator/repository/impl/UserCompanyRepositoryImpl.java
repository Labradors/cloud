package work.labrador.spring.cloud.elevator.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.elevator.UserCompany;
import work.labrador.spring.cloud.elevator.dao.IUserCompanyDao;
import work.labrador.spring.cloud.elevator.repository.MPUserCompanyRepository;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/5 20:41
 */

@Service
public class UserCompanyRepositoryImpl extends ServiceImpl<IUserCompanyDao, UserCompany> implements MPUserCompanyRepository {
}
