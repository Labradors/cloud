package work.labrador.spring.cloud.elevator.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import work.labrador.spring.cloud.common.entity.elevator.Project;

public interface MPProjectRepository extends IService<Project> {
}
