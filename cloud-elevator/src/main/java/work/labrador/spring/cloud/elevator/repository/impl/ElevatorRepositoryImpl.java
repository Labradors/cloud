package work.labrador.spring.cloud.elevator.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.elevator.Elevator;
import work.labrador.spring.cloud.elevator.dao.IElevatorDao;
import work.labrador.spring.cloud.elevator.repository.MPElevatorRepository;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/5 20:41
 */

@Service
public class ElevatorRepositoryImpl extends ServiceImpl<IElevatorDao, Elevator> implements MPElevatorRepository {
}
