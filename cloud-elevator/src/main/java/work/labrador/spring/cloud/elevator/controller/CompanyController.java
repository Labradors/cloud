package work.labrador.spring.cloud.elevator.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.elevator.Company;
import work.labrador.spring.cloud.elevator.repository.impl.CompanyRepositoryImpl;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/5 21:38
 */
@Api(tags = "公司管理", protocols = "http",produces = "application/json", consumes = "application/json")
@RestController
public class CompanyController {

    @Autowired
    CompanyRepositoryImpl companyRepository;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation("创建公司")
    @PostMapping("/company")
    public ResponseEntity<Result<Company>> save(@ApiParam("公司信息") @RequestBody Company company) {
        return ResponseEntity.ok(companyRepository.create(company));
    }

    @ApiOperation("查询公司")
    @GetMapping("/company/{companyId}")
    public ResponseEntity<Result<Company>> read(@ApiParam("公司ID") @PathVariable Long companyId) {
        return ResponseEntity.ok(companyRepository.read(companyId));
    }

    @ApiOperation("更新公司")
    @PutMapping("/company")
    public ResponseEntity<Result<Company>> update(@ApiParam("公司信息") @RequestBody Company company) {
        return ResponseEntity.ok(companyRepository.update(company));
    }


    @ApiOperation("删除公司")
    @DeleteMapping("/company/{companyId}")
    public ResponseEntity<Result<Company>> delete(@ApiParam("公司ID") @PathVariable Long companyId) {
        return ResponseEntity.ok(companyRepository.delete(companyId));
    }


}
