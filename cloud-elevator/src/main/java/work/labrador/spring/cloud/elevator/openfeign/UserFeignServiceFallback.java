package work.labrador.spring.cloud.elevator.openfeign;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysUser;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/8 13:32
 */
@Component
public class UserFeignServiceFallback implements UserFeignService {
    @Override
    public ResponseEntity<Result<SysUser>> readByName(String username) {
        return ResponseEntity.ok(Result.error().msg("熔断降级..."));
    }
}
