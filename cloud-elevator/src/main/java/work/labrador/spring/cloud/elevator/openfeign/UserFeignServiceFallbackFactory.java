package work.labrador.spring.cloud.elevator.openfeign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author Labradors
 * @desc 熔断降级
 * @date 2021/8/7 23:07
 */
@Component
public class UserFeignServiceFallbackFactory implements FallbackFactory<UserFeignService> {

    @Autowired
    UserFeignServiceFallback userFeignServiceFallback;

    @Override
    public UserFeignService create(Throwable cause) {
        return userFeignServiceFallback;
    }
}
