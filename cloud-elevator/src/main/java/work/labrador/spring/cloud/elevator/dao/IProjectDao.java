package work.labrador.spring.cloud.elevator.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import work.labrador.spring.cloud.common.entity.elevator.Project;

@Mapper
public interface IProjectDao extends BaseMapper<Project> {
}
