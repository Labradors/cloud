package work.labrador.spring.cloud.elevator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import javax.annotation.PostConstruct;

/**
 * @author Labradors
 * @desc 电梯管理启动类
 * @date 2021/8/3 22:39
 */
@SpringBootApplication
@EnableAspectJAutoProxy
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan(basePackages = {"work.labrador.spring.cloud"})
public class ElevatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElevatorApplication.class, args);
    }

    @PostConstruct
    public void setProperties(){
        System.setProperty("druid.mysql.usePingMethod","false");
    }
}
