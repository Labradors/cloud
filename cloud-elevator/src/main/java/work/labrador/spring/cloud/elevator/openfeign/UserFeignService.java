package work.labrador.spring.cloud.elevator.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysUser;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/7 0:35
 */
@Component
@FeignClient(value = "SECURITY",path = "/security",fallback = UserFeignServiceFallback.class)
public interface UserFeignService {

    @GetMapping(value = "/sysUser/user/{username}")
    ResponseEntity<Result<SysUser>> readByName(@PathVariable String username);
}
