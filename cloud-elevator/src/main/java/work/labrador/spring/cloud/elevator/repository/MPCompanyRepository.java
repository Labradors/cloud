package work.labrador.spring.cloud.elevator.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.elevator.Company;

public interface MPCompanyRepository extends IService<Company> {

    Result<Company> create(Company company);

    Result<Company> read(Long companyId);

    Result<Company> update(Company company);

    Result<Company> delete(Long companyId);;
}
