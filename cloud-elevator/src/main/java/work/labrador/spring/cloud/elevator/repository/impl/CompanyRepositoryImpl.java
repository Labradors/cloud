package work.labrador.spring.cloud.elevator.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.elevator.Company;
import work.labrador.spring.cloud.common.entity.elevator.UserCompany;
import work.labrador.spring.cloud.common.entity.security.SysUser;
import work.labrador.spring.cloud.elevator.dao.ICompanyDao;
import work.labrador.spring.cloud.elevator.dao.IUserCompanyDao;
import work.labrador.spring.cloud.elevator.openfeign.UserFeignService;
import work.labrador.spring.cloud.elevator.repository.MPCompanyRepository;

import java.util.Objects;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/5 20:38
 */
@Service
@Slf4j
public class CompanyRepositoryImpl extends ServiceImpl<ICompanyDao,Company>  implements MPCompanyRepository {

    @Autowired
    ICompanyDao dao;

    @Autowired
    IUserCompanyDao userCompanyDao;

    @Autowired
    UserFeignService userFeignService;

    @Autowired
    Gson gson;

    @Transactional(timeout = 3,rollbackFor = Exception.class)
    @Override
    public Result<Company> create(Company company) {
        Company build = company.toBuilder().id(null).enable(false).build();
        dao.insert(build);
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ResponseEntity<Result<SysUser>> result = userFeignService.readByName(username);
        if (Objects.requireNonNull(result.getBody()).getData()!=null){
            SysUser userResult = result.getBody().getData();
            userCompanyDao.insert(UserCompany.builder().companyId(build.getId()).userId(userResult.getId()).enable(false).build());
        }
        return Result.ok().data(build);
    }

    @Override
    public Result<Company> read(Long companyId) {
        return Result.ok().data(dao.selectById(companyId));
    }

    @Override
    public Result<Company> update(Company company) {
        if (company.getId()==null){
            return Result.error().msg("ID不能为空...");
        }
        dao.updateById(company);
        return Result.ok().data(company);
    }

    @Transactional(timeout = 10,rollbackFor = Exception.class)
    @Override
    public Result<Company> delete(Long companyId) {
        Company company = dao.selectById(companyId);
        if (company==null){
            return Result.error().msg("没有当前公司信息...");
        }
        dao.deleteById(companyId);
        return Result.ok().msg("删除成功...");
    }
}
