package work.labrador.spring.cloud.elevator.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.elevator.Project;
import work.labrador.spring.cloud.elevator.dao.IProjectDao;
import work.labrador.spring.cloud.elevator.repository.MPProjectRepository;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/5 20:41
 */

@Service
public class ProjectRepositoryImpl extends ServiceImpl<IProjectDao, Project> implements MPProjectRepository {
}
