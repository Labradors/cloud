package work.labrador.spring.cloud.netty;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Jon
 * @desc todo
 * @date 2021年09月08日 23:29
 */
public class NioServerHandler implements Runnable{

    private Selector selector;

    private ServerSocketChannel socketChannel;

    private volatile boolean stop;


    public NioServerHandler(int port){
        try {
            selector = Selector.open();
            socketChannel = ServerSocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.socket().bind(new InetSocketAddress(port),1024);
            socketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("the time server start in port: " + port);
        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }


    @Override
    public void run() {
        while (!stop){
            try {
                // todo 阻塞方法，循环遍历selector,无论是否有读写时间发生，每隔1s selector都会被唤醒一次
                selector.select(1000);
                Iterator<SelectionKey> it = selector.selectedKeys().iterator();
                SelectionKey key = null;
                while (it.hasNext()){
                    key = it.next();
                    it.remove();
                    try {
                        handleInput(key);
                    }catch (IOException e){
                        if (key!=null){
                            key.cancel();
                            if (key.channel()!=null){
                                key.channel().close();
                            }
                        }
                    }
                }
            }catch (Throwable e){
                e.printStackTrace();
            }
        }
    }

    public void handleInput(SelectionKey key) throws IOException{
        if (key.isValid()){
            // 处理新接入的请求
            if (key.isAcceptable()){
                ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                SocketChannel sc = channel.accept();
                sc.configureBlocking(false);
                sc.register(selector,SelectionKey.OP_READ);
            }
            // 接收数据
            if (key.isReadable()){
                SocketChannel sc = (SocketChannel) key.channel();
                ByteBuffer buff = ByteBuffer.allocate(1024);
                int readByte = sc.read(buff);
                if (readByte>0){
                    // 读取到数据
                    buff.flip();
                    byte[] bytes = new byte[buff.remaining()];
                    buff.get(bytes);
                    String body = new String(bytes, StandardCharsets.UTF_8);
                    System.out.println("the time server receive order: "+body);
                    String current = "QUERY TIME ORDER".equalsIgnoreCase(body)?new Date(System.currentTimeMillis()).toString():"BAD ORDER";
                    write(sc,current);
                }else if (readByte < 0){
                    System.out.println("对方关闭了连接...");
                    key.cancel();
                    sc.close();
                }else {
                    System.out.println("没有读取到任何数据...继续循环");
                }
            }
        }
    }

    public void write(SocketChannel channel, String data) throws IOException{
        if (data!=null && data.trim().length()>0){
            byte[] bytes = data.getBytes(StandardCharsets.UTF_8);
            ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
            buffer.put(bytes);
            buffer.flip();
            channel.write(buffer);
        }
    }
}
