package work.labrador.spring.cloud.netty;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author Jon
 * @desc todo
 * @date 2021年09月07日 9:11
 */
public class TimeClient {



    public static void main(String[] args) {
        int port = 8080;
        if (args!=null && args.length>0){
            try {
                port =Integer.parseInt(args[0]);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        Socket socket = null;
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            socket = new Socket("127.0.0.1",port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            while (true) {
                out.println("QUERY TIME ORDER");
                out.flush();
                System.out.println("send order 2 server success");
                String body = in.readLine();
                System.out.println("current time is: " + body);
            }
        }catch (Exception e){
            System.out.println("server happened error..."+e.getMessage());
            e.printStackTrace();
        }finally {
            // 关闭链接
            try {
                if (in!=null){
                    in.close();
                    in = null;
                }
                if (out!=null){
                    out.close();
                    out = null;
                }
                if (socket!=null){
                    socket.close();
                    socket = null;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
