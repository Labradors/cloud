package work.labrador.spring.cloud.netty;

import java.io.*;
import java.net.Socket;
import java.util.Date;

/**
 * @author Jon
 * @desc todo
 * @date 2021年09月06日 15:28
 */
public class TimeServerHandler implements Runnable{

    private Socket socket;

    public TimeServerHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()));
            String currentTime;
            String body;
            while (true){
                System.out.println("...........");
                body = in.readLine();
                System.out.println("cycle read body: "+body);
                if (body==null){
                    break;
                }
                System.out.println("the time server receive order: " + body);
                currentTime = "QUERY TIME ORDER".equalsIgnoreCase(body)?new Date(System.currentTimeMillis()).toString():"BAD ORDER";
                out.println(currentTime);
                // todo 一定要flush
                out.flush();

            }
        }catch (Exception e){
            System.out.println("handler exception: "+e.getMessage());
            try {
                if (in!=null){
                    in.close();
                }
               if (out!=null){
                   out.close();
               }
               if (socket!=null){
                   socket.close();
                   socket = null;
               }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
