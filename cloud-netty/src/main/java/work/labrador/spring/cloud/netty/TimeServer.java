package work.labrador.spring.cloud.netty;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Jon
 * @desc netty时间服务
 * @date 2021年09月06日 14:16
 */
public class TimeServer {


    public static void main(String[] args) {
        int processors = 8;
        ThreadPoolExecutor executor = new ThreadPoolExecutor(processors,processors*2,60L,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(20), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        int port = 8080;
        if (args!=null&&args.length>0){
            try {
                port = Integer.parseInt(args[0]);
            }catch (Exception e){
                System.out.println("参数有误...端口默认值为8080");
            }

        }
        ServerSocket server = null;
        try {
            server = new ServerSocket(port);
            System.out.println("time server is starting in port "+port);
            Socket socket = null;
            while (true){
                socket = server.accept();
                System.out.println("thread starting...");
                executor.execute(new TimeServerHandler(socket));
            }
        } catch (IOException e) {
            executor.shutdown();
            System.out.println("启动线程发生异常...");
            e.printStackTrace();
        }finally {
            if (server!=null){
                System.out.println("time server close");
                try {
                    server.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                server = null;
            }
        }
    }
}
