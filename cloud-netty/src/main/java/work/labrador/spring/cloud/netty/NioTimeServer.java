package work.labrador.spring.cloud.netty;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Jon
 * @desc todo
 * @date 2021年09月08日 23:26
 */
public class NioTimeServer {

    static ThreadPoolExecutor executor = new ThreadPoolExecutor(8,8*2,60L,
            TimeUnit.SECONDS, new LinkedBlockingDeque<>(20), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {
        int port = 8080;
        if (args!=null&&args.length>0){
            try {
                port = Integer.parseInt(args[0]);
            }catch (NumberFormatException e){
                e.printStackTrace();
            }
        }
        executor.execute(new NioServerHandler(port));
    }
}
