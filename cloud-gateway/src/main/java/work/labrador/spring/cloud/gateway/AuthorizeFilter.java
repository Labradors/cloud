package work.labrador.spring.cloud.gateway;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import work.labrador.spring.cloud.common.entity.Result;

import java.util.Arrays;

/**
 * @author Labradors
 * @desc 全局jwt认证
 * @date 2021/8/4 17:40
 */
@Component
@Slf4j
public class AuthorizeFilter implements GlobalFilter, Ordered {

    @Autowired
    Gson gson;
    @Value("${jwt.header}")
    String header;
    @Value("${jwt.tokenHead}")
    String tokenHead;
    @Value("${exclude}")
    String[] exclude;
    @Autowired
    JwtTokenUtil tokenUtil;
    @Autowired
    RedisBasedSessionUtil redisBasedSessionUtil;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String path = request.getPath().value();
        if (!Arrays.asList(exclude).contains(path)) {
            HttpHeaders headers = request.getHeaders();
            String token = headers.getFirst(header);
            if (StringUtils.isBlank(token)) {
                String body = gson.toJson(Result.error().msg("token为空..."));
                ServerHttpResponse response = exchange.getResponse();
                DataBuffer buffer = response.bufferFactory().wrap(body.getBytes());
                response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
                return response.writeWith(Mono.just(buffer));
            }
            token = token.replace(tokenHead,"").trim();
            String username = tokenUtil.getUsernameFromToken(token);
            String authToken = redisBasedSessionUtil.getTokenInSession(username);
            if (StringUtils.isBlank(authToken) || !authToken.equals(token)) {
                String body = gson.toJson(Result.error().msg("token不匹配,请重新登录..."));
                ServerHttpResponse response = exchange.getResponse();
                DataBuffer buffer = response.bufferFactory().wrap(body.getBytes());
                response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
                return response.writeWith(Mono.just(buffer));
            }
        }
        log.info(path+"通过token检查");
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -1;
    }
}
