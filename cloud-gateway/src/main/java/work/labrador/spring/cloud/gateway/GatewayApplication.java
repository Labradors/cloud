package work.labrador.spring.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/4 0:07
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableWebFlux
@ComponentScan(value = {"work.labrador.spring.cloud.common.config","work.labrador.spring.cloud.gateway"})
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
