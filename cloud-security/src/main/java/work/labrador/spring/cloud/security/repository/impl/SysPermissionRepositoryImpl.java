package work.labrador.spring.cloud.security.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysPermission;
import work.labrador.spring.cloud.common.entity.security.SysUser;
import work.labrador.spring.cloud.common.entity.security.enums.PermissionEnum;
import work.labrador.spring.cloud.security.dao.ISysPermissionDao;
import work.labrador.spring.cloud.security.dao.ISysRoleDao;
import work.labrador.spring.cloud.security.dao.ISysRolePermissionDao;
import work.labrador.spring.cloud.security.repository.MPSysPermissionRepository;
import work.labrador.spring.cloud.security.utils.TreeUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
@Service
public class SysPermissionRepositoryImpl extends ServiceImpl<ISysPermissionDao, SysPermission> implements MPSysPermissionRepository {

    @Autowired
    ISysPermissionDao permissionDao;

    @Autowired
    ISysRolePermissionDao rolePermissionDao;

    @Autowired
    ISysRoleDao roleDao;


    @Override
    public Result<SysPermission> save(Long id, Long fatherId, int type, String name, String mark, String describes) {
        SysPermission permission = null;
        if (id!=null){
            permission = permissionDao.selectById(id);
            if (permission==null){
                return Result.error("参数错误...");
            }
            permission = SysPermission.builder().id(id).fatherId(fatherId).type(PermissionEnum.getPermission(type))
                    .name(name).mark(mark).describes(describes).build();
            permissionDao.updateById(permission);
        }else {
            permission = SysPermission.builder().fatherId(fatherId).type(PermissionEnum.getPermission(type)).name(name)
                    .mark(mark).describes(describes).build();
            permissionDao.insert(permission);
        }
        return Result.ok(permission);
    }

    @Override
    public Result<Set<SysPermission>> selectUserPermission() {
        SysUser user=(SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<String> roles = Arrays.stream(user.getRoles().split(",")).collect(Collectors.toList());
        Long roleId = roleDao.selectByMark(roles.get(0));
        Set<SysPermission> copy = rolePermissionDao.selectRolePermission(roleId);
        return Result.ok(TreeUtil.findRoots(copy));
    }

    @Override
    public Result<Set<SysPermission>> selectRolePermission(Long roleId) {
        Set<SysPermission> copy = rolePermissionDao.selectRolePermission(roleId);
        return Result.ok(TreeUtil.findRoots(copy));
    }

}
