package work.labrador.spring.cloud.security.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysPermission;
import work.labrador.spring.cloud.common.entity.security.SysRolePermission;
import work.labrador.spring.cloud.security.dao.ISysRolePermissionDao;
import work.labrador.spring.cloud.security.repository.MPSysRolePermissionRepository;
import work.labrador.spring.cloud.security.utils.TreeUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
@Service
public class SysRolePermissionRepositoryImpl extends ServiceImpl<ISysRolePermissionDao, SysRolePermission> implements MPSysRolePermissionRepository {

    @Autowired
    ISysRolePermissionDao rolePermissionDao;


    @Override
    public Result saveRolePermission(Long roleId, Long[] permissionId) {
        rolePermissionDao.deleteRolePermission(roleId);
        List<SysRolePermission> rolePermissions = Lists.newArrayList();
        Arrays.stream(permissionId).forEach(id->rolePermissions.add(SysRolePermission.builder().roleId(roleId).permissionId(id).build()));
        return Result.ok().data(saveBatch(rolePermissions)?"处理成功...":"处理失败...");
    }

    @Override
    public Result<Set<SysPermission>> selectPermissions(Long roleId) {
        Set<SysPermission> copy = new CopyOnWriteArraySet<>(rolePermissionDao.selectRolePermission(roleId));
        return Result.ok().data(TreeUtil.findRoots(copy));
    }

    @Override
    public Result<Set<SysPermission>> updateRolePermission(Long roleId, Set<Long> permissions) {
        Set<Long> permissionId = rolePermissionDao.selectRolePermissionId(roleId);
        Set<Long> set = Sets.newHashSet();
        set.addAll(permissionId);
        set.retainAll(permissionId);
        permissions.removeAll(set);
        List<SysRolePermission> add = Lists.newArrayList();
        permissions.forEach(permission->add.add(SysRolePermission.builder().roleId(roleId).permissionId(permission).build()));
        saveOrUpdateBatch(add);
        permissionId.removeAll(set);
        permissionId.forEach(permission->remove(query().getWrapper().eq("role_id",roleId).eq("permission_id",permission)));
        return selectPermissions(roleId);
    }
}
