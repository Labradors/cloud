package work.labrador.spring.cloud.security.controller;


import com.google.common.collect.Sets;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysPermission;
import work.labrador.spring.cloud.security.repository.MPSysPermissionRepository;
import work.labrador.spring.cloud.security.repository.MPSysRolePermissionRepository;

import java.util.Set;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Labradors
 * @since 2021-07-10
 */
@Api(tags = "权限管理", protocols = "http", produces = "application/json", consumes = "application/json")
@RestController
@RequestMapping("/sysPermission")
public class SysPermissionController {

    @Autowired
    MPSysPermissionRepository pr;

    @Autowired
    MPSysRolePermissionRepository rolePermissionRepository;

    @ApiOperation("添加权限")
    @PostMapping("/add")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Result<SysPermission>> addPermission(@ApiParam("权限ID") Long id, @ApiParam("父级权限ID") @RequestParam("fatherId") Long fatherId,
                                                               @ApiParam("权限类型") @RequestParam("type") int type, @ApiParam("权限名称") @RequestParam("name") String name,
                                                               @ApiParam("权限标识") @RequestParam("mark") String mark, @ApiParam("权限描述") @RequestParam("describes") String describes ){
        return ResponseEntity.ok(pr.save(id,fatherId,type,name,mark,describes));
    }

    @ApiOperation("查询用户权限")
    @GetMapping("/permission")
    public ResponseEntity<Result<Set<SysPermission>>> selectPermission(){
        return ResponseEntity.ok(pr.selectUserPermission());
    }

    @ApiOperation("查询角色对应权限")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/permission/{roleId}")
    public ResponseEntity<Result<Set<SysPermission>>> selectRolePermission(@PathVariable Long roleId){
        return ResponseEntity.ok(pr.selectRolePermission(roleId));
    }

    @ApiOperation("为角色分配权限")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/permission/{roleId}")
    public ResponseEntity<Result<Set<SysPermission>>> updateRolePermission(@PathVariable Long roleId, Long[] permissionIds){
        return ResponseEntity.ok(rolePermissionRepository.updateRolePermission(roleId, Sets.newHashSet(permissionIds)));
    }

}

