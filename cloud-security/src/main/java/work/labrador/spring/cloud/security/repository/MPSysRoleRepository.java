package work.labrador.spring.cloud.security.repository;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.extension.service.IService;
import work.labrador.spring.cloud.common.entity.security.SysRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
public interface MPSysRoleRepository extends IService<SysRole> {

}
