package work.labrador.spring.cloud.security.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysPermission;

import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
public interface MPSysPermissionRepository extends IService<SysPermission> {
    // 保存权限
    Result<SysPermission> save(Long id, Long fatherId, int type, String name, String mark, String describes);

    // 用户查询自己的查询权限
    Result<Set<SysPermission>> selectUserPermission();

    // 根据角色查询权限
    Result<Set<SysPermission>> selectRolePermission(Long roleId);

}
