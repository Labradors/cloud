package work.labrador.spring.cloud.security.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.security.core.userdetails.UserDetails;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
public interface MPSysUserRepository extends IService<SysUser> {
    Result<SysUser> save(String username, String password, String mobile);

    UserDetails loadUserByMobile(String mobile);

    Result<SysUser> login(String username,String password);

    Result<SysUser> readByName(String username);
}
