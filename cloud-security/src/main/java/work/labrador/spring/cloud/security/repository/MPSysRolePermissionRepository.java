package work.labrador.spring.cloud.security.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysPermission;
import work.labrador.spring.cloud.common.entity.security.SysRolePermission;

import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Labradors
 * @since 2021-07-10
 */
public interface MPSysRolePermissionRepository extends IService<SysRolePermission> {
    // 先删除所有权限，再为角色添加权限
    Result saveRolePermission(Long roleId, Long[] permissionId);

    // 根据角色ID查询所有权限并进行分组
    Result<Set<SysPermission>> selectPermissions(Long roleId);

    // 给角色配置权限
    Result<Set<SysPermission>> updateRolePermission(Long roleId, Set<Long> permissions);
}
