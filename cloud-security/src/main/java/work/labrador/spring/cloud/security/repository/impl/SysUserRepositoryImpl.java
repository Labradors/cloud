package work.labrador.spring.cloud.security.repository.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysUser;
import work.labrador.spring.cloud.security.dao.ISysUserDao;
import work.labrador.spring.cloud.security.repository.MPSysUserRepository;
import work.labrador.spring.cloud.security.utils.StringUtil;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
@Service
public class SysUserRepositoryImpl extends ServiceImpl<ISysUserDao, SysUser> implements MPSysUserRepository, UserDetailsService {

    @Autowired
    ISysUserDao userDao;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RedisBasedSessionUtil redisBasedSessionUtil;

    @Override
    public Result<SysUser> save(String username, String password, String mobile) {
        SysUser user = SysUser.builder().mobile(mobile).username(username).password(new BCryptPasswordEncoder().encode(password)).roles(null).build();
        userDao.insert(user);
        return Result.ok().data(user);
    }

    @Override
    public UserDetails loadUserByMobile(String mobile) {
        // TODO: 2021/8/5 可以从redis优化用户权限
        return userDao.selectOne(query().getWrapper().eq("mobile",mobile));
    }

    @Override
    public Result<SysUser> login(String username, String password) {
        UserDetails userDetails;
        SysUser user;
        if (StringUtil.isMobile(username)){
            userDetails = loadUserByMobile(username);
        }else {
            userDetails = loadUserByUsername(username);
        }
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), password);
        Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = redisBasedSessionUtil.getTokenInSession(username);
        if (StringUtils.isBlank(token)) {
            token = jwtTokenUtil.generateToken(userDetails);
            redisBasedSessionUtil.addSessionObject(token, userDetails);
        }
        if (StringUtil.isMobile(username)){
            user = userDao.selectOne(query().getWrapper().eq("mobile",username));
        }else {
            user = userDao.selectOne(query().getWrapper().eq("username",username));
        }
        user.setToken(token);
        return Result.ok().data(user);
    }

    @Override
    public Result<SysUser> readByName(String username) {
        return Result.ok().data(userDao.selectOne(query().getWrapper().eq("username",username)));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        // TODO: 2021/8/5 可以从redis优化用户权限
        return userDao.selectOne(query().getWrapper().eq("username",s));
    }
}
