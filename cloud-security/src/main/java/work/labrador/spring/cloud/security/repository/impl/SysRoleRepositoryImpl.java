package work.labrador.spring.cloud.security.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.entity.security.SysRole;
import work.labrador.spring.cloud.security.dao.ISysRoleDao;
import work.labrador.spring.cloud.security.repository.MPSysRoleRepository;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
@Service
public class SysRoleRepositoryImpl extends ServiceImpl<ISysRoleDao, SysRole> implements MPSysRoleRepository {

}
