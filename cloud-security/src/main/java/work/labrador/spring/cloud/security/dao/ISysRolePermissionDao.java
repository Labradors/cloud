package work.labrador.spring.cloud.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import work.labrador.spring.cloud.common.entity.security.SysPermission;
import work.labrador.spring.cloud.common.entity.security.SysRolePermission;

import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Labradors
 * @since 2021-07-10
 */
@Mapper
public interface ISysRolePermissionDao extends BaseMapper<SysRolePermission> {

    @Delete("delete from sys_role_permission where role_id = #{roleId}")
    void deleteRolePermission(Long roleId);


    @Select("select p.id, p.father_id, p.type, p.name, p.mark, p.describes from sys_role_permission rp inner join sys_permission p on rp.permission_id = p.id where rp.role_id = #{roleId}")
    Set<SysPermission> selectRolePermission(Long roleId);

    @Select("select p.permission_id from sys_role_permission where role_id = #{roleId}")
    Set<Long> selectRolePermissionId(Long roleId);



}
