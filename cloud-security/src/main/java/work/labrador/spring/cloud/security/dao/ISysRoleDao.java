package work.labrador.spring.cloud.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import work.labrador.spring.cloud.common.entity.security.SysRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
@Mapper
public interface ISysRoleDao extends BaseMapper<SysRole> {

    // 根据角色名称查询角色ID
    @Select("select r.id from sys_role r where r.mark = #{mark}")
    Long selectByMark(String mark);

}
