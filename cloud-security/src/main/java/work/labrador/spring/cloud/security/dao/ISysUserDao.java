package work.labrador.spring.cloud.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import work.labrador.spring.cloud.common.entity.security.SysUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Erwin Feng
 * @since 2021-07-10
 */
@Mapper
public interface ISysUserDao extends BaseMapper<SysUser> {



}
