package work.labrador.spring.cloud.security.utils;


import work.labrador.spring.cloud.common.entity.security.SysPermission;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

public class TreeUtil {

    public static Set<SysPermission> findRoots(Set<SysPermission> allNodes) {
        // 根节点
        Set<SysPermission> root;
        root = allNodes.stream().filter(sysPermission -> sysPermission.getFatherId()==null).collect(Collectors.toSet());
        root.forEach(node -> {
            findChildren(node, allNodes);
        });
        return root;
    }

    /**
     * 递归查找子节点
     *
     * @param treeNodes
     * @return
     */
    private static SysPermission findChildren(SysPermission node, Set<SysPermission> treeNodes) {
        for (SysPermission it : treeNodes) {
            if (node.getId().equals(it.getFatherId())) {
                if (node.getChildren() == null) {
                    node.setChildren(new ArrayList<>());
                }
                node.getChildren().add(findChildren(it, treeNodes));
            }
        }
        return node;
    }
}
