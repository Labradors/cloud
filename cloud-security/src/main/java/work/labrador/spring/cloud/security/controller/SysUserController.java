package work.labrador.spring.cloud.security.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import work.labrador.spring.cloud.common.entity.Result;
import work.labrador.spring.cloud.common.entity.security.SysUser;
import work.labrador.spring.cloud.security.repository.impl.SysUserRepositoryImpl;

import java.util.Random;

/**
 * <p>
 *  用户管理
 * </p>
 *
 * @author Labradors
 * @since 2021-07-10
 */
@Api(tags = "用户管理", protocols = "http",produces = "application/json", consumes = "application/json")
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    SysUserRepositoryImpl userRepository;

    @ApiOperation("添加用户")
    @PostMapping("/save")
    public ResponseEntity<Result<SysUser>> save(@ApiParam("用户名") @RequestParam("username") String username,
                                                @ApiParam("密码") @RequestParam("password") String password,
                                                @ApiParam("手机号") @RequestParam("mobile") String mobile){
        return ResponseEntity.ok(userRepository.save(username,password,mobile));
    }
    @ApiOperation("用户登录")
    @PostMapping("/login")
    public ResponseEntity<Result<SysUser>> login(@ApiParam("用户名") @RequestParam("username") String username,
                                                 @ApiParam("密码") @RequestParam("password") String password){
        return ResponseEntity.ok(userRepository.login(username,password));
    }

    @ApiOperation("根据用户名查询用户信息")
    @GetMapping("/user/{username}")
    public ResponseEntity<Result<SysUser>> readByName(@PathVariable String username){
        return ResponseEntity.ok(userRepository.readByName(username));
    }


}

