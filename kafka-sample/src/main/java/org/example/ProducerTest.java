package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ProducerTest.confirm();
    }

    /**
     * confirm
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void confirm() throws ExecutionException, InterruptedException {
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.2.5:9092,192.168.2.6:9092,192.168.2.7:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("acks", "-1");
        props.put("retries", 3);
        props.put("batch.size", 323840);
        props.put("linger.ms", 10);
        props.put("buffer.memory", 33554432);
        props.put("max.block.ms", 3000);
        KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        for (int i = 0; i < 100; i++) {
            String value = String.valueOf(i);
            // 同步发送
            // producer.send(new ProducerRecord<>("checkCluster", value)).get();
            // 异步发送
            producer.send(new ProducerRecord<>("checkCluster", value), (metadata, exception) -> {
                // 消息投递回调，如果异常不为空，则进行消息处理
                if (exception != null) {
                    // 重新投递消息
                    exception.printStackTrace();
                }else {
                    System.out.printf("消息: %s 发送成功!%n",value);
                }
            });
        }
        producer.close();
    }
}
