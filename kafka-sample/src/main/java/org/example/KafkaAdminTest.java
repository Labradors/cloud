package org.example;

import com.google.gson.Gson;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class KafkaAdminTest {
    private static final String TOPIC = "checkCluster";
    private static final String SERVER = "192.168.2.5:9092,192.168.2.6:9092,192.168.2.7:9092";

    public static void main(String[] args) throws InterruptedException {
        Properties properties = new Properties();
        properties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, SERVER);
        try(AdminClient adminClient = AdminClient.create(properties)){
            Gson gson = new Gson();
            DescribeClusterResult result = adminClient.describeCluster();
            List<String> topics = new ArrayList<>();
            topics.add("clinic.order");
            topics.add("clinic.recipe");
            DescribeTopicsResult topicsResult = adminClient.describeTopics(topics);
            System.out.println(gson.toJson(topicsResult.all().get()));
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }

    }


}
