package org.example;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collections;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ConsumerTest {
    private static final String TOPIC = "checkCluster";
    private static final String SERVER = "192.168.2.5:9092,192.168.2.6:9092,192.168.2.7:9092";

    public static void main(String[] args) throws InterruptedException {
        manualConsumer();
        // autoCommitConsumer();
    }

    /**
     * auto commit
     * @throws InterruptedException 连接中断
     */
    public static void autoCommitConsumer() throws InterruptedException {
        Properties props = new Properties();
        // kafka集群
        props.put("bootstrap.servers",SERVER);
        //props.put("bootstrap.servers","192.168.2.5:9092,192.168.2.6:9092,192.168.2.7:9092");
        // 群组ID，多消费者节点群组ID相同
        props.put("group.id","cluster");
        // 自动commit管理
        props.put("enable.auto.commit", true);
        props.put("auto.commit.interval.ms", 1000);
        props.put("auto.offset.reset","earliest");
        props.put("max.poll.records",100);
        // 消息key序列化器
        props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        // 消息value序列化器
        props.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        // 构建KafkaConsumer
        KafkaConsumer<String,String> consumer = new KafkaConsumer<>(props);
        // 订阅主题
        consumer.subscribe(Collections.singletonList(TOPIC));
        int i = 0;
        while (true) {
            System.out.println(i);
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (TopicPartition partition : records.partitions()) {
                for (ConsumerRecord<String, String> record : records.records(partition)) {
                    System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
                    TimeUnit.MILLISECONDS.sleep(100);
                }
            }
            i++;
        }
    }


    /**
     * 手动提交
     * @throws InterruptedException 连接中断
     */
    public static void manualConsumer() throws InterruptedException {
        Properties props = new Properties();
        // kafka集群
        props.put("bootstrap.servers",SERVER);
        //props.put("bootstrap.servers","192.168.2.5:9092,192.168.2.6:9092,192.168.2.7:9092");
        // 群组ID，多消费者节点群组ID相同
        props.put("group.id","cluster");
        // 自动commit管理
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,false);
        // 从最早的消息开始读取
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,100);
        // 消息key序列化器
        props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        // 消息value序列化器
        props.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        // 构建KafkaConsumer
        try (KafkaConsumer<String,String> consumer = new KafkaConsumer<>(props)){
            // 订阅主题
            consumer.subscribe(Collections.singletonList(TOPIC));
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                for (TopicPartition partition : records.partitions()) {
                    for (ConsumerRecord<String, String> record : records.records(partition)) {
                        System.out.printf("offset = %d, partition = %d, value = %s%n", record.offset(), partition.partition(), record.value());
                        TimeUnit.MILLISECONDS.sleep(50);
                    }
                    // 同步提交，会阻塞并重试
                    // consumer.commitSync();
                    // 异步提交，不会阻塞并且不会重试,可以使用一个递增序列控制提交序列，如果失败的提交是小的位移，大的位移已经成功，则可以忽略，否则就需要进行重试
                    consumer.commitAsync((offsets, exception) -> {
                        if (Objects.isNull(exception)){
                            System.out.println("异步提交成功...");
                        }else {
                            System.out.println("异步提交失败,偏移量: "+ offsets +",异常信息: " + exception);
                        }
                    });
                    // k再均衡是指分区的所属权从一个消费者转移到另一消费者的行为，它为消费组具备高可用
                    //性和伸缩性提供保障，使我们可以既方便又安全地删除消费组内的消费者或往消费组内添加消
                    //费者。不过在再均衡发生期间，消费组内的消费者是无法读取消息的。 也就是说，在再均衡发
                    //生期间的这一小段时 间 内，消费组会变得不可用 。另 外，当一个分区被重新分配给另一个消费
                    //者时，
                    //消费者当前的状态也会丢失。比如消费者消费完某个分区中的一部分消息时还没有来得
                    //及提交消费位移就发生了再均衡操作 ，
                    //之后这个分区又被分配给了消费组 内 的另一个消费者，
                    //原来被消费完的那部分消息又被重新消费一遍，也就是发生了重复消费。一般情况下，应尽量
                    //避免不必要的再均衡的发生。

                    
                }
            }
        }
    }
}
