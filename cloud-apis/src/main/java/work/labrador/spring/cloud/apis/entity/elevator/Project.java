package work.labrador.spring.cloud.apis.entity.elevator;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.annotation.Lazy;
import work.labrador.spring.cloud.apis.entity.elevator.enums.ProjectType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/4 22:28
 */
@Lazy
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@TableName("elevator_project")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Project extends Model<Project> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "项目ID",dataType = "Long")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "项目名称",dataType = "String")
    @TableField(value = "name")
    @NotBlank(message = "项目名称不能为空")
    private String name;

    @ApiModelProperty(value = "项目地址",dataType = "String")
    @TableField(value = "address")
    @NotBlank(message = "项目地址不能为空")
    private String address;

    @ApiModelProperty(value = "项目类型,0:住宅,1:医院,2:政府部门,3:学校,4:商业用房")
    @TableField(value = "type")
    @NotBlank(message = "项目类型不能为空")
    private ProjectType type;

    @ApiModelProperty(value = "项目联系电话",dataType = "String")
    @TableField(value = "mobile")
    @NotBlank(message = "项目联系电话不能为空")
    @Length(min = 11, max = 11, message = "手机号只能为11位")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String mobile;

    @ApiModelProperty(value = "公司省份ID",dataType = "Long")
    @TableField(value = "province_id")
    @NotBlank(message = "省不能为空")
    private Long provinceId;

    @ApiModelProperty(value = "市ID",dataType = "Long")
    @TableField(value = "city_id")
    @NotBlank(message = "市不能为空")
    private Long cityId;

    @ApiModelProperty(value = "区ID",dataType = "Long")
    @TableField(value = "country_id")
    @NotBlank(message = "区不能为空")
    private Long countryId;

    @ApiModelProperty(value = "使用单位ID",dataType = "Long")
    @TableField(value = "use_unit_id")
    @NotBlank(message = "使用单位ID不能为空")
    private Long useUnitId;

    @ApiModelProperty(value = "维保单位ID",dataType = "Long")
    @TableField(value = "service_unit_id")
    private Long serviceUnitId;

    @ApiModelProperty(value = "创建时间",dataType = "Long")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "更新时间",dataType = "Long")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "是否可用",dataType = "Boolean")
    @TableField(value = "enable")
    private boolean enable;
}
