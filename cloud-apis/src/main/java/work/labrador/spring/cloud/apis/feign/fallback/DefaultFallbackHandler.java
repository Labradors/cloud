package work.labrador.spring.cloud.apis.feign.fallback;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.google.common.collect.Maps;
import work.labrador.spring.cloud.apis.entity.test.Good;

import java.util.List;
import java.util.Map;

/**
 * @author 姜陶
 * @date 2022/12/15 13:37
 * @describe 默认降级处理器
 **/
public class DefaultFallbackHandler {

    public static Map<Long, Good> blockGoodsByIds(List<Long> goodIds, BlockException e) {
        System.out.println("资源被流控");
        Map<Long,Good> degrade = Maps.newHashMap();
        degrade.put(10L,Good.builder().id(10L).name("我擦").build());
        return degrade;
    }

    //public static Map<Long, Good> fallbackGoodsByIds(List<Long> goodIds,Throwable e) {
    //    System.out.println("日本人");
    //    Map<Long,Good> goodMap = Maps.newHashMap();
    //    goodMap.put(1L,Good.builder().id(1L).name("麻花").build());
    //    goodMap.put(2L,Good.builder().id(2L).name("麻辣").build());
    //    goodMap.put(3L,Good.builder().id(3L).name("麻婆豆腐").build());
    //    return goodMap;
    //}

    public static Map<Long, Good> fallbackGoodsByIds(List<Long> goodIds,Throwable e) {
        System.out.println("日本人");
        Map<Long,Good> goodMap = Maps.newHashMap();
        goodMap.put(3L,Good.builder().id(3L).name("麻婆豆腐").build());
        return goodMap;
    }
}
