package work.labrador.spring.cloud.apis.entity.elevator.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.Getter;

@Getter
public enum CompanyType implements IEnum<Integer> {
    SERVICE_UNIT(0,"维保企业"),
    USE_UNIT(1, "使用单位"),
    PROD_UNIT(2, "制造单位"),
    CHECK_UNIT(3, "检验单位"),
    MANAGE_UNIT(4, "监管单位"),
    PROTECT_UNIT(5, "保险单位");

    @EnumValue
    private final int value;
    private final String desc;

    CompanyType(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
