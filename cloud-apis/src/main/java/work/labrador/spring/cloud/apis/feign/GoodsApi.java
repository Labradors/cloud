package work.labrador.spring.cloud.apis.feign;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import work.labrador.spring.cloud.apis.entity.test.Good;
import work.labrador.spring.cloud.apis.feign.fallback.DefaultFallbackHandler;
import work.labrador.spring.cloud.apis.feign.fallback.GoodsApiFallBack;

import java.util.List;
import java.util.Map;

/**
 * @author 姜陶
 * @date 2022/12/12 11:32
 * @describe 用户相关API
 **/
@FeignClient(
        name = "provider",
        contextId = "provider",
        path = "provider"
        //fallback = GoodsApiFallBack.class
)
public interface GoodsApi {

    /**
     * 根据商品ID列表查询商品信息
     *
     * @param goodIds 商品ID列表
     * @return 商品信息
     */
    @GetMapping("/goods/ids")
    Map<Long, Good> getGoodsByIds(@RequestParam List<Long> goodIds);
}
