package work.labrador.spring.cloud.apis.entity.elevator.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.Getter;

@Getter
public enum ElevatorType implements IEnum<Integer> {
    WIRE_ROPE(0,"曳引电梯"),
    RACK_PINION(1,"扶梯"),
    CHAIN_SPROCKET(2,"杂物电梯"),
    HYDRAULIC(3,"液压电梯");;
    @EnumValue
    private final int value;
    private final String desc;


    ElevatorType(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return null;
    }
}
