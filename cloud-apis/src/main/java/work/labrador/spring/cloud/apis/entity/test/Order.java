package work.labrador.spring.cloud.apis.entity.test;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;
import org.springframework.context.annotation.Lazy;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Lazy
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@TableName("test_order")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Alias("order")
public class Order extends Model<Order> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Id
     */
    @ApiModelProperty(value = "订单ID",dataType = "Long")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID",dataType = "Long")
    @TableField(value = "good_id")
    @NotBlank(message = "商品ID不能为空")
    private Long goodId;


    /**
     * 商品ID
     */
    @ApiModelProperty(value = "用户ID",dataType = "Long")
    @TableField(value = "user_id")
    @NotBlank(message = "用户ID不能为空")
    private Long userId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "Long")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Long createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "Long")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /**
     * 是否可用
     */
    @ApiModelProperty(value = "是否可用",dataType = "Boolean")
    @TableField(value = "enable")
    private boolean enable;
}
