package work.labrador.spring.cloud.apis.entity;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/7 0:48
 */
@Configuration
public class FeignConfiguration {

    @Bean
    Logger.Level gitHubFeignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
