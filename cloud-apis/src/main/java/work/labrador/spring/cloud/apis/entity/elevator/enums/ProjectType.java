package work.labrador.spring.cloud.apis.entity.elevator.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.Getter;

@Getter
public enum ProjectType implements IEnum<Integer>{
    HOUSE(0,"住宅"),
    HOSPITAL(1,"医院"),
    GOVERNMENT(2,"政府部门"),
    SCHOOL(3,"学校"),
    BUSINESS(4,"商业用房");

    @EnumValue
    private final int value;
    private final String desc;

    ProjectType(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Integer getValue() {
        return value;
    }

}
