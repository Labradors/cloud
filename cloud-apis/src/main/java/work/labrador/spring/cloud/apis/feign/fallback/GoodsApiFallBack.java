package work.labrador.spring.cloud.apis.feign.fallback;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.apis.entity.test.Good;
import work.labrador.spring.cloud.apis.feign.GoodsApi;

import java.util.List;
import java.util.Map;

/**
 * @author 姜陶
 * @date 2022/12/12 11:40
 * @describe TODO
 **/
@Service
public class GoodsApiFallBack implements GoodsApi {
    @Override
    public Map<Long, Good> getGoodsByIds(List<Long> goodIds) {
        System.out.println("日本人");
        Map<Long,Good> goodMap = Maps.newHashMap();
        goodMap.put(1L,Good.builder().id(1L).name("麻花").build());
        goodMap.put(2L,Good.builder().id(2L).name("麻辣").build());
        goodMap.put(3L,Good.builder().id(3L).name("麻婆豆腐").build());
        return goodMap;
    }
}
