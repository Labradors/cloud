package work.labrador.spring.cloud.apis.entity.elevator;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.context.annotation.Lazy;
import work.labrador.spring.cloud.apis.entity.elevator.enums.ElevatorType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/4 22:28
 */
@Lazy
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@TableName("elevator_elevator")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Elevator  extends Model<Elevator> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "公司ID",dataType = "Long")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "电梯类型,0:曳引电梯,1:扶梯,2:杂物电梯,3:液压电梯")
    @TableField(value = "type")
    @NotBlank(message = "电梯类型不能为空")
    private ElevatorType type;

    @ApiModelProperty(value = "设备代码",dataType = "String")
    @TableField(value = "device_code")
    @NotBlank(message = "设备代码不能为空")
    private String deviceCode;

    @ApiModelProperty(value = "电梯编号",dataType = "String")
    @TableField(value = "number")
    @NotBlank(message = "电梯编号不能为空")
    private String number;

    @ApiModelProperty(value = "制造单位",dataType = "String")
    @TableField(value = "prod_unit")
    private String prodUnit;

    @ApiModelProperty(value = "年检单位",dataType = "String")
    @TableField(value = "check_unit")
    @NotBlank(message = "年检单位不能为空")
    private String checkUnit;

    @ApiModelProperty(value = "使用单位ID",dataType = "Long")
    @TableField(value = "use_unit_id")
    @NotBlank(message = "使用单位ID不能为空")
    private Long useUnitId;

    @ApiModelProperty(value = "维保单位ID",dataType = "Long")
    @TableField(value = "service_unit_id")
    @NotBlank(message = "维保单位ID不能为空")
    private Long serviceUnitId;

    @ApiModelProperty(value = "维保人员ID",dataType = "Long")
    @TableField(value = "service_user_id")
    private Long serviceUserId;

    @ApiModelProperty(value = "项目ID",dataType = "Long")
    @TableField(value = "project_id")
    @NotBlank(message = "项目ID不能为空")
    private Long projectId;

    @ApiModelProperty(value = "项目内编号",dataType = "String")
    @TableField(value = "inner_code")
    @NotBlank(message = "项目内编号不能为空")
    private String innerCode;

    @ApiModelProperty(value = "梯龄",dataType = "Integer")
    @TableField(value = "age")
    @NotBlank(message = "梯龄不能为空")
    @Max(value = 100,message = "梯龄过大")
    @Min(value = 0,message = "梯龄不能小于0")
    private Integer age;

    @ApiModelProperty(value = "速度",dataType = "Integer")
    @TableField(value = "speed")
    @NotBlank(message = "速度不能为空")
    @Max(value = 10,message = "速度过大")
    @Min(value = 0,message = "速度不能小于0")
    private Integer speed;

    @ApiModelProperty(value = "创建时间",dataType = "Long")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "更新时间",dataType = "Long")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "是否可用",dataType = "Boolean")
    @TableField(value = "enable")
    private boolean enable;
}
