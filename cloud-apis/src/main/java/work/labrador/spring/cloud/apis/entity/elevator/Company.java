package work.labrador.spring.cloud.apis.entity.elevator;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.annotation.Lazy;
import work.labrador.spring.cloud.apis.entity.elevator.enums.CompanyType;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/4 18:36
 */
@Lazy
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@TableName("elevator_company")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Company extends Model<Company> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "公司ID",dataType = "Long")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "公司名称",dataType = "String")
    @TableField(value = "name")
    @NotBlank(message = "公司名称不能为空")
    private String name;

    @ApiModelProperty(value = "公司地址",dataType = "String")
    @TableField(value = "address")
    @NotBlank(message = "公司地址不能为空")
    private String address;

    @ApiModelProperty(value = "公司邮箱",dataType = "String")
    @TableField(value = "email")
    @NotBlank(message = "公司邮箱")
    @Email(message = "邮箱格式不正确")
    private String email;

    @ApiModelProperty(value = "公司联系电话",dataType = "String")
    @TableField(value = "mobile")
    @NotBlank(message = "公司联系电话不能为空")
    @Length(min = 11, max = 11, message = "手机号只能为11位")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String mobile;

    @ApiModelProperty(value = "公司省份ID",dataType = "Long")
    @TableField(value = "province_id")
    @NotBlank(message = "省不能为空")
    private Long provinceId;

    @ApiModelProperty(value = "市ID",dataType = "Long")
    @TableField(value = "city_id")
    @NotBlank(message = "市不能为空")
    private Long cityId;

    @ApiModelProperty(value = "区ID",dataType = "Long")
    @TableField(value = "country_id")
    @NotBlank(message = "区不能为空")
    private Long countryId;

    @ApiModelProperty(value = "公司类型,0:维保单位,1:使用单位,2:制造单位,3:检验单位,4:监管单位,5:保险单位")
    @TableField(value = "type")
    @NotBlank(message = "公司类型不能为空")
    private CompanyType type;

    @ApiModelProperty(value = "创建时间",dataType = "Long")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "更新时间",dataType = "Long")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "是否可用",dataType = "Boolean")
    @TableField(value = "enable")
    private boolean enable;

}
