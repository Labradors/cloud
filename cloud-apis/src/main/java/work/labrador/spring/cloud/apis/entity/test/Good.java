package work.labrador.spring.cloud.apis.entity.test;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;
import org.springframework.context.annotation.Lazy;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Lazy
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("test_good")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Alias("good")
public class Good extends Model<Good> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Id
     */
    @ApiModelProperty(value = "商品ID",dataType = "Long")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称",dataType = "String")
    @TableField(value = "name")
    @NotBlank(message = "商品名称不能为空")
    private String name;


    /**
     * 商品库存
     */
    @ApiModelProperty(value = "库存数量",dataType = "Long")
    @TableField(value = "stock")
    @NotBlank(message = "库存数量不能为空")
    private Long stock;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "Long")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Long createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "Long")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /**
     * 是否可用
     */
    @ApiModelProperty(value = "是否可用",dataType = "Boolean")
    @TableField(value = "enable")
    private boolean enable;
}
