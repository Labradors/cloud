package work.labrador.spring.cloud.apis.entity.elevator;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.context.annotation.Lazy;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Labradors
 * @desc TODO
 * @date 2021/8/4 22:27
 */
@Lazy
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@TableName("elevator_user_company")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserCompany extends Model<UserCompany> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID",dataType = "Long")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "用户ID",dataType = "Long")
    @TableField(value = "user_id")
    @NotBlank(message = "用户ID不能为空")
    private Long userId;

    @ApiModelProperty(value = "公司ID",dataType = "Long")
    @TableField(value = "company_id")
    @NotBlank(message = "公司ID不能为空")
    private Long companyId;

    @ApiModelProperty(value = "创建时间",dataType = "Long")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "更新时间",dataType = "Long")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "是否可用",dataType = "Boolean")
    @TableField(value = "enable")
    private boolean enable;
}
