package work.labradors.sentinel.consumer.controller;

import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import work.labrador.spring.cloud.apis.entity.test.Good;
import work.labrador.spring.cloud.apis.feign.GoodsApi;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 姜陶
 * @date 2022/12/12 11:46
 * @describe TODO
 **/
@RestController
public class ConsumerController {
    @Resource
    private GoodsApi goodsApi;

    @GetMapping("/charge/wait")
    public Map<Long, Good> selectWaitCharge() {
        return goodsApi.getGoodsByIds(Lists.newArrayList(1L, 2L));
    }

    @GetMapping("/it")
    public String it() {
        System.out.println("阿西吧");
        return "阿西吧";
    }
}
