package work.labrador.spring.cloud.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import work.labrador.spring.cloud.elasticsearch.entity.Article;

/**
 * @author Jon
 * @desc article repo
 * @date 2022年02月01日 23:34
 */
@Repository
public interface ArticleRepository extends ElasticsearchRepository<Article,Long> {


}
