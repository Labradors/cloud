package work.labrador.spring.cloud.elasticsearch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import work.labrador.spring.cloud.common.Result;
import work.labrador.spring.cloud.elasticsearch.entity.Article;
import work.labrador.spring.cloud.elasticsearch.service.impl.ArticleService;

/**
 * @author Jon
 * @desc article api
 * @date 2022年02月01日 23:40
 */
@RestController
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @PostMapping(value = "/article/insert")
    public Result<Article> insertAtc(String title, String content){
        return articleService.insert(title,content);
    }

    @GetMapping(value = "/article/get")
    public Result<Article> insertAtc(Long id){
        return articleService.select(id);
    }

    @GetMapping(value = "/article/all")
    public Result<Iterable<Article>> findAll(){
        return articleService.findAll();
    }

    @GetMapping(value = "/article/page")
    public Result<Page<Article>> findPage(Integer page, Integer size){
        return articleService.findPage(page,size);
    }

    @GetMapping(value = "/article/title/page")
    public Result<Page<SearchHit<Article>>> findPageTitle(String title, Integer page, Integer size){
        return articleService.findByTitlePage(title,page,size);
    }

    @GetMapping(value = "/article/content/page")
    public Result<Page<SearchHit<Article>>> findPageContent(String content, Integer page, Integer size){
        return articleService.findByContentPage(content,page,size);
    }
}
