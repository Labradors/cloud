package work.labrador.spring.cloud.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Jon
 * @desc Elastic研究
 * @date 2022年02月01日 20:52
 */
@SpringBootApplication(scanBasePackages = {"work.labrador.spring.cloud"})
public class ElasticApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticApplication.class,args);
    }
}
