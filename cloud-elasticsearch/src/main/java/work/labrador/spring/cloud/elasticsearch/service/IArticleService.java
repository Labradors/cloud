package work.labrador.spring.cloud.elasticsearch.service;

import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import work.labrador.spring.cloud.common.Result;
import work.labrador.spring.cloud.elasticsearch.entity.Article;

/**
 * @author Jon
 * @desc 文章service interface
 * @date 2022年02月01日 23:42
 */
public interface IArticleService {

    /**
     * 插入数据
     * @param title 标题
     * @param content 内容
     * @return {@link Article}
     */
    Result<Article> insert(String title,String content);

    /**
     * 根据ID查询文章
     * @param id id
     * @return {@link Article}
     */
    Result<Article> select(Long id);

    /**
     * 查询所有文章
     * @return {@link Iterable<Article>}
     */
    Result<Iterable<Article>> findAll();

    /**
     * 查询所有内容
     * @param page page
     * @param size size
     * @return {@link Result}
     */
    Result<Page<Article>> findPage(Integer page,Integer size);

    /**
     * 根据title模糊查询
     * @param title title
     * @param page page
     * @param size size
     * @return {@link Result}
     */
    Result<Page<SearchHit<Article>>> findByTitlePage(String title, Integer page, Integer size);

    /**
     * 根据content模糊查询
     * @param content content
     * @param page page
     * @param size size
     * @return {@link Result}
     */
    Result<Page<SearchHit<Article>>> findByContentPage(String content, Integer page, Integer size);
}
