package work.labrador.spring.cloud.elasticsearch.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import work.labrador.spring.cloud.common.Result;
import work.labrador.spring.cloud.elasticsearch.entity.Article;
import work.labrador.spring.cloud.elasticsearch.repository.ArticleRepository;
import work.labrador.spring.cloud.elasticsearch.service.IArticleService;
import work.labrador.spring.cloud.common.*;

import java.util.Optional;

/**
 * @author Jon
 * @desc 文章服务实现
 * @date 2022年02月01日 23:44
 */
@Service
@Slf4j
public class ArticleService implements IArticleService {

    @Autowired
    ArticleRepository articleRepo;

    @Autowired
    ElasticsearchRestTemplate restTemplate;

    @Override
    public Result<Article> insert(String title, String content) {
        Long id = SnowFlake.snakeId();
        log.info("id为: {}",id);
        Article atc = Article.builder().id(id).author(SnowFlake.snakeId()).title(title)
                .content(content).build();
        articleRepo.save(atc);
        return Result.ok(atc);
    }

    @Override
    public Result<Article> select(Long id) {
        Optional<Article> option = articleRepo.findById(id);
        if (option.isPresent()){
            return Result.ok(option.get());
        }
        return Result.error();
    }

    @Override
    public Result<Iterable<Article>> findAll() {
        return Result.ok(articleRepo.findAll());
    }

    @Override
    public Result<Page<Article>> findPage(Integer page, Integer size) {
        return Result.ok(articleRepo.findAll(PageRequest.of(page,size)));
    }

    @Override
    public Result<Page<SearchHit<Article>>> findByTitlePage(String title, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("title",title))
                .withPageable(pageable)
                .build();
        SearchHits<Article> sa = restTemplate.search(query,Article.class);
        return Result.ok(new PageImpl<SearchHit<Article>>(sa.getSearchHits(),pageable,sa.getTotalHits()));
    }

    @Override
    public Result<Page<SearchHit<Article>>> findByContentPage(String content, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchQuery("content",content))
                .withPageable(pageable)
                .build();
        SearchHits<Article> sa = restTemplate.search(query,Article.class);

        return Result.ok(new PageImpl<SearchHit<Article>>(sa.getSearchHits(),pageable,sa.getTotalHits()));
    }
}
