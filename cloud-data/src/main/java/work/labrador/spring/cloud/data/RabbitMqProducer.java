package work.labrador.spring.cloud.data;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import work.labrador.spring.cloud.common.Result;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Jon
 * @desc todo
 * @date 2021年10月29日 13:03
 */
@Component
@Slf4j
public class RabbitMqProducer {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    KafkaTemplate<String,String> kafkaTemplate;

    @Autowired
    Gson gson;

    @Autowired
    RabbitMqConfirmCallback confirmCallback;

    @Autowired
    RabbitMqReturnCallback returnCallback;

    private String ex1 = "ex1";
    private String ex2 = "ex2";
    private String route_key1 = "route_key1";
    private String route_key2 = "route_key2";
    private volatile AtomicInteger i = new AtomicInteger(0);
    private volatile AtomicInteger j = new AtomicInteger(0);

    //@Scheduled(fixedRate = 300)
    public void productMsg1(){
        rabbitTemplate.convertAndSend(ex1,"", Result.ok(i.toString()));
        i.incrementAndGet();
    }

    //@Scheduled(fixedRate = 10000)
    public void productMsg2(){
        /**
         * 确保消息发送失败后可以重新返回到队列中
         * 注意：yml需要配置 publisher-returns: true
         */
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setConfirmCallback(confirmCallback);
        rabbitTemplate.setReturnsCallback(returnCallback);
        CorrelationData uuid = new CorrelationData(UUID.randomUUID().toString());
        rabbitTemplate.convertAndSend(ex2,"", Result.error(j.toString()),message -> {
                message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                return message;
        },uuid);
        log.info("ex2消息发送成功，uuid={}",uuid.getId());
        j.incrementAndGet();
    }


    //@Scheduled(fixedRate = 300)
    public void productKafkaMsg1(){
        kafkaTemplate.send(ex1,"", gson.toJson(Result.ok(i.toString())));
        i.incrementAndGet();
    }


}
