package work.labrador.spring.cloud.data;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.common.cache.Cache;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

import javax.annotation.Resource;

/**
 * Kafka生产者
 */
@Component
@Slf4j
public class KafkaProducer {

    private static final String TOPIC = "checkCluster";

    @Resource(name = "guavaCache")
    private Cache<Long,Object> guavaCache;

    @Resource
    KafkaTemplate<Long,String> kafkaTemplate;

    @Scheduled(fixedRate = 300)
    public void sendNoticeMsg(){
        kafkaTemplate.setProducerListener(new ProducerListener<Long, String>() {
            @Override
            public void onSuccess(ProducerRecord<Long, String> producerRecord, RecordMetadata recordMetadata) {
                ProducerListener.super.onSuccess(producerRecord, recordMetadata);
            }

            @Override
            public void onError(ProducerRecord<Long, String> producerRecord, RecordMetadata recordMetadata, Exception exception) {
                ProducerListener.super.onError(producerRecord, recordMetadata, exception);
            }
        });
        long key = IdWorker.getId();
        String value = "你好,测试Spring Kafka!"+key;
        ProducerRecord<Long,String> record = new ProducerRecord<Long,String>(TOPIC,key,value);
        guavaCache.put(key,value);
        kafkaTemplate.send(record).addCallback(new ListenableFutureCallback<SendResult<Long, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("发送消息失败,消息内容:{},失败原因: {}", JSON.toJSONString(record),ex);
                // 写库

                // 删除缓存
                guavaCache.invalidate(key);
            }

            @Override
            public void onSuccess(SendResult<Long, String> result) {
                guavaCache.invalidate(key);
                String topic = result.getRecordMetadata().topic();
                long offset = result.getRecordMetadata().offset();
                int partition = result.getRecordMetadata().partition();
                log.info("发送成功：topic->{},partition->{} , offset->{},msg->{}",topic,partition,offset,result.getProducerRecord().value());
            }
        });
    }
}
