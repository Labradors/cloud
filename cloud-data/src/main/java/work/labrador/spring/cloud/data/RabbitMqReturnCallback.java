package work.labrador.spring.cloud.data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Jon
 * @desc todo
 * @date 2021年11月26日 10:50
 */
@Component
@Slf4j
public class RabbitMqReturnCallback implements RabbitTemplate.ReturnsCallback {

    @Override
    public void returnedMessage(ReturnedMessage returned) {
        log.info("消息未能投递到目标queue，重新投递, returnedMessage ===> replyCode={} ,replyText={} ,exchange={} ,routingKey={}",
                returned.getReplyCode(), returned.getReplyText(), returned.getExchange(), returned.getRoutingKey());
    }
}
