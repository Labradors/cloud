package work.labrador.spring.cloud.data;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.concurrent.TimeUnit;

/**
 * @author 姜陶
 * @date 2022/7/14 13:46
 * @describe 本地缓存配置
 **/
@Configuration
public class GuavaCacheConfig {
    // 默认1千条
    private static final Integer initialCapacity = 1000;
    // 最大1亿数据
    private static final Long maximumSize = 100000000L;

    @Lazy
    @Bean("guavaCache")
    Cache<Long,Object> guavaCache(){
        return CacheBuilder.newBuilder()
                .concurrencyLevel(Runtime.getRuntime().availableProcessors()*2)
                .expireAfterAccess(1, TimeUnit.HOURS)
                .initialCapacity(initialCapacity)
                .maximumSize(maximumSize)
                .recordStats()
                .build();
    }

    @Lazy
    @Bean("gatewayCache")
    Cache<Object,Object> gatewayCache(){
        return CacheBuilder.newBuilder()
                .concurrencyLevel(Runtime.getRuntime().availableProcessors()*2)
                .expireAfterAccess(1, TimeUnit.HOURS)
                .initialCapacity(initialCapacity)
                .maximumSize(maximumSize)
                .recordStats()
                .build();
    }
}
