package work.labrador.spring.cloud.data;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import work.labrador.spring.cloud.common.Result;

import java.nio.charset.StandardCharsets;

/**
 * @author Jon
 * @desc todo
 * @date 2021年10月29日 23:29
 */
@Component
@Slf4j
public class RabbitMqConsumer {

    private static final String ex1 = "ex1";
    private static final String ex2 = "ex2";
    private static final String route_key1 = "route_key1";
    private static final String route_key2 = "route_key2";
    private static final String cache = "cache";

    @Autowired
    Gson gson;

    @RabbitListener(queues = route_key1)
    //@RabbitHandler
    public void consumeKey1(Result msg, Channel channel, Message message) throws Exception {
        try {
            log.info("客户端1收到消息，{}",msg.toString());
            int a = 1/0;
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            log.error("消息即将再次返回队列处理...");
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
        }

    }

    //@RabbitListener(queues = route_key2)
    //@RabbitHandler
    public void consumeEx1(Result msg, Channel channel, Message message) throws Exception{
        try {
            log.info("客户端1收到消息，{}, {}",message.toString(),msg.toString());
            int a = 1/0;
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            /*log.error("消息即将再次返回队列处理...");直接放到队列头部，导致cpu打死
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);*/
            // 自动确认
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            // 手动将消息放到队尾
            channel.basicPublish(message.getMessageProperties().getReceivedExchange(),
                    message.getMessageProperties().getReceivedRoutingKey(), MessageProperties.PERSISTENT_TEXT_PLAIN,
                    gson.toJson(msg).getBytes(StandardCharsets.UTF_8));
            //设置消息重复发送次数

        }
    }


    @RabbitListener(queues = cache)
    @RabbitHandler
    public void consumeCanal(Channel channel, Message message) throws Exception{
        try {
            log.info("客户端1收到消息，{}",new String(message.getBody(),"UTF-8"));
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            /*log.error("消息即将再次返回队列处理...");直接放到队列头部，导致cpu打死
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);*/
            // 自动确认
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
            // 手动将消息放到队尾
            channel.basicPublish(message.getMessageProperties().getReceivedExchange(),
                    message.getMessageProperties().getReceivedRoutingKey(), MessageProperties.PERSISTENT_TEXT_PLAIN,
                    new String(message.getBody()).getBytes(StandardCharsets.UTF_8));
            //设置消息重复发送次数

        }
    }

    @KafkaListener(topics = {ex1})
    public void consumeKafkaEx1(ConsumerRecord<String,String> result) throws Exception{
        log.info("kafka route {} is arrive, msg is {}",ex1,result.toString());
        //ack.acknowledge();
    }
}
