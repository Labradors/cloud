package work.labrador.spring.cloud.data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Jon
 * @desc todo
 * @date 2021年11月26日 10:47
 */
@Component
@Slf4j
public class RabbitMqConfirmCallback  implements RabbitTemplate.ConfirmCallback {
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (!ack){
            log.error("消息投递exchange异常,重新投递,cause={}",cause);
        }else {
            log.info("发送到exchange成功，correlationData={} ,ack={}, cause={}", correlationData.getId(), true, cause);
        }
    }
}
