package work.labradors.sentinel.provider.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import work.labrador.spring.cloud.apis.entity.test.Good;
import work.labrador.spring.cloud.apis.feign.GoodsApi;
import work.labrador.spring.cloud.apis.feign.fallback.DefaultFallbackHandler;

import java.util.List;
import java.util.Map;

/**
 * @author 姜陶
 * @date 2022/12/12 11:43
 * @describe 测试限流
 **/
@RestController
@Slf4j
public class GoodsController implements GoodsApi {

    //@SentinelResource(blockHandler = "blockGoodsByIds",fallback = "fallbackGoodsByIds",value = "goods")
    //@SentinelResource(blockHandler = "blockGoodsByIds", fallback = "fallback",fallbackClass = DefaultFallbackHandler.class)
    @SentinelResource(blockHandler = "blockGoodsByIds",blockHandlerClass = DefaultFallbackHandler.class,
            fallback = "fallbackGoodsByIds", fallbackClass =DefaultFallbackHandler.class, value = "goods")
    @GetMapping("/goods/ids")
    @Override
    public Map<Long, Good> getGoodsByIds(@RequestParam List<Long> goodIds) {
        //Map<Long,Good> goodMap = Maps.newHashMap();
        //goodMap.put(1L,Good.builder().id(1L).name("麻花").build());
        //return goodMap;
        throw new RuntimeException("手动异常!");
    }


    // TODO: 2022/12/13 默认流控处理器 
    public Map<Long, Good> fallbackGoodsByIds(List<Long> goodIds,Throwable e) {
        System.out.println("日本人");
        Map<Long,Good> goodMap = Maps.newHashMap();
        goodMap.put(1L,Good.builder().id(1L).name("麻花").build());
        goodMap.put(2L,Good.builder().id(2L).name("麻辣").build());
        goodMap.put(3L,Good.builder().id(3L).name("麻婆豆腐").build());
        return goodMap;
    }
}
