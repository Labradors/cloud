package work.labrador.spring.cloud.test.repository.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import work.labrador.spring.cloud.test.repository.RedisRepository;


@SpringBootTest
class RedisRepositoryImplTest {

    @Autowired
    RedisRepository redisRepository;

    @Test
    void setList() {
        redisRepository.setList();
    }

    @Test
    void range() {
        redisRepository.range();
    }

    @Test
    void pop(){
        redisRepository.pop();
    }

    @Test
    void index(){
        redisRepository.index();
    }

    @Test
    void setMap(){
        redisRepository.setMap();
    }

    @Test
    void  setSet(){
        redisRepository.setSet();
    }

    @Test
    void getMap(){
        redisRepository.getMap();
    }

    @Test
    void getSet(){
        redisRepository.getSet();
    }
}