drop table if exists test_good;
CREATE TABLE `test_good`  (
                                     `id` BIGINT(20) NOT NULL COMMENT '主键ID',
                                     `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '公司名称',
                                     `stock` BIGINT(20) NOT NULL COMMENT '库存数量',
                                     `create_time` BIGINT(10) NOT NULL COMMENT '创建时间',
                                     `update_time` BIGINT(10) NOT NULL COMMENT '修改时间',
                                     `enable` BOOLEAN NOT NULL DEFAULT TRUE COMMENT '是否可用',
                                     PRIMARY KEY (`id`) USING BTREE

) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

drop table if exists test_order;
CREATE TABLE `test_order`  (
                                          `id` BIGINT(20) NOT NULL PRIMARY KEY COMMENT '主键ID',
                                          `good_id` BIGINT(20) NOT NULL COMMENT '商品ID',
                                          `user_id` BIGINT(20) NOT NULL COMMENT '商品ID',
                                          `create_time` BIGINT(10) NOT NULL COMMENT '创建时间',
                                          `update_time` BIGINT(10) NOT NULL COMMENT '修改时间',
                                          `enable` BOOLEAN NOT NULL DEFAULT TRUE COMMENT '是否可用'

) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;