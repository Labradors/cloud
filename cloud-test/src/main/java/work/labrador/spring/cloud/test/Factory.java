package work.labrador.spring.cloud.test;

/**
 * @author Jon
 * @desc 工厂模式
 * @date 2022年02月15日 21:16
 */
public class Factory {
    // 简单工厂
    interface Phone{
        public void make();
    }

    class XiaoMiPhone implements Phone{

        @Override
        public void make() {
            System.out.println("生成小米手机");
        }
    }

    class IPhone implements Phone{

        @Override
        public void make() {
            System.out.println("生产苹果手机...");
        }
    }

    class PhoneFactory{
        public Phone makePhone(int phoneType){
            if (phoneType == 1) {
                return new IPhone();
            }
            return new XiaoMiPhone();
        }
    }

    //抽象工厂
    interface AbstractFactory{
        Phone makePhone();
    }

    class XiaoMiFactory implements AbstractFactory{

        @Override
        public Phone makePhone() {
            return new XiaoMiPhone();
        }
    }

    class IPhoneFactory implements AbstractFactory{

        @Override
        public Phone makePhone() {
            return new IPhone();
        }
    }
}
