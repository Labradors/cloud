package work.labrador.spring.cloud.test.utils;

import java.util.ArrayList;

/**
 * @author 姜陶
 * @date 2022/2/9 11:50
 * @describe 自定义栈
 **/
public class ListNode {
    private String v;
    private ListNode next;

    public ListNode(){}

    public ListNode(String v) {
        this.v = v;
    }

    public ListNode(String v, ListNode next) {
        this.v = v;
        this.next = next;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public static ListNode node(ArrayList<String> v,int i){
        ListNode node = new ListNode(v.get(i));
        if (i<v.size()-1) {
            i = i + 1;
            node.setNext(node(v, i));
        }
        return node;
    }

    /**
     * 根据现有node添加元素
     * @param node 链表
     * @param v 数组
     * @param i 当前节点
     * @return {@link ListNode}
     */
    public static ListNode node(ListNode node,ArrayList<String> v,int i){
        while (node.next!=null){
            node=node.next;
        }
        if (node.v==null){
            return node(v,i);
        }
        node.setNext(node(v,i));
        return node;
    }

    @Override
    public String toString() {
        ListNode node = this;
        StringBuilder buffer = new StringBuilder();
        while (node.next!=null){
            buffer.append(node.v);
            node = node.next;
        }
        buffer.append(node.v);
        return buffer.toString();
    }
}
