package work.labrador.spring.cloud.test.utils;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @author 姜陶
 * @date 2022/2/15 16:00
 * @describe 自定义锁
 **/
public class SmartLock implements Lock,Serializable {
    private Sync sync;
    public SmartLock(){
        sync = new Sync();
    }
    // 内部需要实现AQS
    private static class Sync extends AbstractQueuedSynchronizer{
        final void lock(){
            acquire(1);
        }

        @Override
        protected boolean tryAcquire(int arg) {
            if (compareAndSetState(0,1)){
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        @Override
        protected boolean tryRelease(int arg) {
            if (getState()==0){
                return true;
            }else {
                if (compareAndSetState(arg,0)){
                    setExclusiveOwnerThread(null);
                    return true;
                }
            }
            return false;
        }

        @Override
        protected boolean isHeldExclusively() {
            return getState()==1;
        }

        final ConditionObject newCondition() {
            return new ConditionObject();
        }
    }
    @Override
    public void lock() {
        sync.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        sync.acquireInterruptibly(1);
    }

    @Override
    public boolean tryLock() {
        return sync.tryAcquire(1);
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return sync.tryAcquireNanos(1,unit.toNanos(time));
    }

    @Override
    public void unlock() {
        sync.release(1);
    }

    @Override
    public Condition newCondition() {
        return sync.newCondition();
    }
}
