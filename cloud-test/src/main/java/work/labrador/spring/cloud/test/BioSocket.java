package work.labrador.spring.cloud.test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Jon
 * @desc BIO编程
 * @date 2022年02月16日 22:15
 */
public class BioSocket {
    private static final int PORT = 8080;
    private static ServerSocket serverSocket;
    private static final ExecutorService executor = Executors.newFixedThreadPool(16);

    public static void main(String[] args) {

    }

    private static synchronized void start(){
        try {
            serverSocket = new ServerSocket(PORT);
            while (true){
                Socket socket = serverSocket.accept();
                // 启动新线程执行连接任务
                executor.submit(new SocketHandler(socket));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class SocketHandler implements Runnable{
        private Socket socket;

        public SocketHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            // 执行io读写等操作
        }
    }
}
