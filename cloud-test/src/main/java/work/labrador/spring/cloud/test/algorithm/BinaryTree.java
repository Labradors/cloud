package work.labrador.spring.cloud.test.algorithm;

/**
 * @author Jon
 * @desc 二叉树操作集合
 * @date 2021年08月18日 23:51
 */
public class BinaryTree {


    /**
     * 先序遍历二叉树
     * @param root 二叉树
     */
    public void traverse(TreeNode root) {
        if (root != null) {
            System.out.println(root.val);
            traverse(root.left);
            traverse(root.right);
        }
    }

    /**
     * 中序遍历二叉树
     * @param root 二叉树
     */
    public void traverseMiddle(TreeNode root) {
        if (root != null) {
            traverseMiddle(root.left);
            System.out.println(root.val);
            traverseMiddle(root.right);
        }
    }

    /**
     * 后续遍历二叉树
     * @param root 二叉树
     */
    public void traverseLast(TreeNode root) {
        if (root != null) {
            traverseLast(root.left);
            traverseLast(root.right);
            System.out.println(root.val);
        }
    }


    /**
     * 利用数组快速创建二叉树
     * @param array 数组
     * @param index 当前index值
     * @return
     */
    public TreeNode createBinaryTree(int[] array, int index) {
        TreeNode treeNode = null;
        if (index < array.length) {
            treeNode = new TreeNode(array[index]);
            // 对于顺序存储的完全二叉树，如果某个节点的索引为index，其对应的左子树的索引为2*index+1，右子树为2*index+1
            treeNode.left = createBinaryTree(array, 2 * index + 1);
            treeNode.right = createBinaryTree(array, 2 * index + 2);
        }
        return treeNode;

    }


}
