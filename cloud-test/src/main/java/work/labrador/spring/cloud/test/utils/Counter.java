package work.labrador.spring.cloud.test.utils;

/**
 * @author 姜陶
 * @date 2022/2/16 16:42
 * @describe 计数器示例
 **/
public class Counter {
    private static final SmartLock lock = new SmartLock();
    private int ntm = 0;

    public void add(){
        lock.lock();
        try {
            ntm = ntm+1;
        }finally {
            lock.unlock();
        }
    }

    public void decre(){
        lock.lock();
        try {
            ntm = ntm -1;
        }finally {
            lock.unlock();
        }
    }

    public int get(){
        lock.lock();
        try {
            return ntm;
        }finally {
            lock.unlock();
        }
    }
}
