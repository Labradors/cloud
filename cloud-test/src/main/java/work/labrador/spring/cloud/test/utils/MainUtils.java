package work.labrador.spring.cloud.test.utils;

import com.google.gson.Gson;
import work.labrador.spring.cloud.common.Result;

import java.util.LinkedList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author 姜陶
 * @date 2022/2/8 13:02
 * @describe 原子包的使用
 **/
public class MainUtils {
    public static AtomicInteger integer = new AtomicInteger(0);
    public static AtomicReference<Result> res = new AtomicReference<>();
    public static AtomicStampedReference<Integer> sar;
    public static AtomicMarkableReference<Integer> amr;
    public static int natm = 0;
    private static ReentrantLock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();
    private static final LinkedList<Integer> link = new LinkedList<>();
    private static final CountDownLatch latch = new CountDownLatch(4);
    private static CyclicBarrier barrier = null;
    private static final CopyOnWriteArrayList<Long> co = new CopyOnWriteArrayList<>();
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    private static final Semaphore sma = new Semaphore(10);
    private static final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    private static final ReentrantReadWriteLock.ReadLock rLock = rwLock.readLock();
    private static final ReentrantReadWriteLock.WriteLock wLock = rwLock.writeLock();
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();
        executor.submit(() -> {
            for (int i=0;i<100;i++){
                counter.add();
            }
        });
        executor.submit(() -> {
            for (int i=0;i<100;i++){
                counter.add();
            }
        });
        Thread.sleep(5000);
        System.out.println(counter.get());
        executor.shutdown();
    }



    public static void rwLockTest(){
        for (int i=0;i<90;i++){
            write(i);
        }
        executor.submit(() -> {
            for (int i = 0;i<100;i++){
                System.out.println(read(i));
            }
        });
        executor.submit(() -> {
            for (int i = 0;i<10;i++){
                write(i);
            }
        });
    }

    public static void write(Integer value){
        wLock.lock();
        try {
            link.offerLast(value);
        }finally {
            wLock.unlock();
        }
    }

    public static Integer read(int index){
        rLock.lock();
        Integer v;
        try {
            v = link.get(index);
        }finally {
            rLock.unlock();
        }
        return v;
    }

    public static void semaphore(){
        executor.submit(() -> {
            for (int i=0;i<20;i++){
                executor.submit(come());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executor.submit(() -> {
            for (int i=0;i<20;i++){
                executor.submit(go());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Runnable come(){
        return () -> {
            try {
                sma.acquire();
                if (sma.availablePermits()<=0){
                    System.out.println("来了一辆车,当前空余车位:"+sma.availablePermits()+",等待中...");
                }else {
                    System.out.println("来了一辆车,当前空余车位:"+sma.availablePermits());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
    }

    public static Runnable go(){
        return () -> {
            sma.release();
            System.out.println("走了一辆车,当前空余车位:"+sma.availablePermits());
        };
    }



    public static void barriar1(){
        barrier = new CyclicBarrier(4, () -> {
            System.out.println("上车旅行去咯...");
            try {
                Thread.sleep(3000);
                executor.shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });
        executor.submit(pack1("爸爸"));
        executor.submit(pack1("妈妈"));
        executor.submit(pack1("妻子"));
        executor.submit(pack1("孩子"));
    }

    public void barriarMain(){
        barrier = new CyclicBarrier(10, () -> {
            long v = co.stream().mapToLong(value -> (value)).sum();
            System.out.println("统计结果: "+v);
        });
        cyclicMain();
        try {
            Thread.sleep(5000);
            co.clear();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cyclicMain();
        try {
            Thread.sleep(5000);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void cyclicMain(){
        for (int i=0;i<100000;i=i+10000){
            executor.submit(cyclic(i,i+10000));
        }
    }

    public static Runnable cyclic(int start,int end){
        return () -> {
            long v = 0;
            for (int i=start;i<end;i++){
                v= v+i;
            }
            co.add(v);
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        };
    }

    public static void countDown(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.submit(pack("爸爸"));
        executor.submit(pack("妈妈"));
        executor.submit(pack("妻子"));
        executor.submit(pack("孩子"));
        try {
            latch.await();
            executor.submit(() -> System.out.println("上车旅行去咯..."));
            Thread.sleep(3000);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static Runnable pack(String name){
        return () -> {
            try {
                Thread.sleep(1000);
                System.out.println(name + "收拾完行李了...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                latch.countDown();
            }
        };
    }

    public static Runnable pack1(String name){
        return () -> {
            try {
                Thread.sleep(1000);
                System.out.println(name + "收拾完行李了...");
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        };
    }



    public static void  reentrantLock(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.submit(() -> {
            lock.lock();
            try {
                for (int i=0;i<100;i++){
                    natm = natm+1;
                }
            }finally {
                lock.unlock();
            }
        });
        executor.submit(() -> {
            lock.lock();
            try {
                for (int i=0;i<100;i++){
                    natm = natm+1;
                }
            }finally {
                lock.unlock();
            }
        });
        try {
            Thread.sleep(3000);
            System.out.println(natm);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void  reentrantTryLock(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.submit(() -> {
            while (true){
                if (lock.tryLock()){
                    try {
                        for (int i=0;i<100;i++){
                            natm = natm+1;
                        }
                        break;
                    }finally {
                        lock.unlock();
                    }
                }
            }
        });
        executor.submit(() -> {
            while (true){
                if (lock.tryLock()){
                    try {
                        for (int i=0;i<100;i++){
                            natm = natm+1;
                        }
                        break;
                    }finally {
                        lock.unlock();
                    }
                }
            }
        });
        try {
            Thread.sleep(3000);
            System.out.println(natm);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void  reentrantTryLock1(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.submit(() -> {
            while (true){
                try {
                    if (lock.tryLock(1, TimeUnit.SECONDS)){
                        try {
                            for (int i=0;i<100;i++){
                                natm = natm+1;
                            }
                            break;
                        }finally {
                            lock.unlock();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    if (lock.isHeldByCurrentThread()) {
                        lock.unlock();
                    }
                }
            }
        });
        executor.submit(() -> {
            executor.submit(() -> {
                while (true){
                    try {
                        if (lock.tryLock(1, TimeUnit.SECONDS)){
                            try {
                                for (int i=0;i<100;i++){
                                    natm = natm+1;
                                }
                                break;
                            }finally {
                                lock.unlock();
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally {
                        if (lock.isHeldByCurrentThread()) {
                            lock.unlock();
                        }
                    }
                }
            });
        });
        try {
            Thread.sleep(3000);
            System.out.println(natm);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void  reentrantLockCondition(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        ExecuteQueue queue = new ExecuteQueue();
        executor.submit(() -> {
            while (true){
                System.out.println(queue.getTask());
            }
        });
        executor.submit(() -> {
            queue.addInteger(100);
        });

        try {
            Thread.sleep(10000);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void longAdd(){
        LongAdder adder = new LongAdder();
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.submit(() -> {
            for (int i=0;i<100;i++){
                adder.add(1);
            }
        });
        executor.submit(() -> {
            for (int i=0;i<100;i++){
                adder.add(1);
            }
        });
        try {
            Thread.sleep(3000);
            System.out.println(adder.sumThenReset());
            adder.increment();
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void atomicMarkableReference(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        amr = new AtomicMarkableReference<>(0,false);
        executor.submit(() -> System.out.println("1. 更改成功: "+amr.compareAndSet(0,
                1,amr.isMarked(),true)));
        executor.submit(() -> System.out.println("1. 更改成功: "+amr.compareAndSet(1,
                0,amr.isMarked(),true)));
        System.out.println(amr.getReference());
        System.out.println(amr.isMarked());
        try {
            Thread.sleep(3000);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void atomicStampedReferenceTest(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        sar = new AtomicStampedReference<>(0,0);
        executor.submit(() -> {
            System.out.println("1. 更改成功: "+sar.compareAndSet(0,
                    1,sar.getStamp(),sar.getStamp()+1));
        });
        executor.submit(() -> {
            int stamp = sar.getStamp();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("2. 更改成功: "+sar.compareAndSet(0,1,
                    stamp,stamp+1));
        });
        executor.submit(() -> {
            System.out.println("3. 更改成功: "+sar.compareAndSet(1,
                    0,sar.getStamp(),sar.getStamp()+1));
        });

        try {
            Thread.sleep(3000);
            System.out.println(sar.getReference());
            System.out.println(sar.getStamp());
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }



    public static void atomicReferenceTest(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        Result v = Result.ok();
        executor.submit(() -> res.set(v));
        executor.submit(() -> res.compareAndSet(v,Result.error()));
        System.out.println(new Gson().toJson(res.get()));
        executor.shutdown();
    }

    /**
     * AtomicInteger测试
     */
    public static void atomicIntegerTest(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        //AtomicMarkableReference
        //AtomicReference
        //AtomicStampedReference
        executor.submit(() -> {
            for (int i = 0;i<10000;i++){
                integer.incrementAndGet();
            }
            return integer.get();
        });
        executor.submit(() -> {
            for (int i = 0;i<10000;i++){
                integer.incrementAndGet();
            }
            return integer.get();
        });
        executor.submit(() -> {
            for (int i = 0;i<10000;i++){
                natm +=1;
            }
        });
        executor.submit(() -> {
            for (int i = 0;i<10000;i++){
                natm +=1;
            }
        });
        try {
            Thread.sleep(6000);
            System.out.println(integer.get());
            System.out.println(natm);
            executor.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
