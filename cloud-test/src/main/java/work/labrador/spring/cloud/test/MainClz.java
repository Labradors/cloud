package work.labrador.spring.cloud.test;


import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MainClz {

    private static final ReentrantLock lock = new ReentrantLock();
    private static final Condition cdt = lock.newCondition();
    private static final ExecutorService es = Executors.newCachedThreadPool();
    private static final LinkedList<Integer> ll = new LinkedList<>();
    public static void main(String[] args) {
        ConditionClass cc = new ConditionClass();
        es.submit(() -> {
            for (int i = 0;i<100;i++){
                cc.set(i);
            }
        });
        es.submit(() -> {
            while (true){
                System.out.println(cc.get());
            }
        });
    }

    static class ConditionClass {
        public Integer get(){
            lock.lock();
            try {
                if (ll.isEmpty()){
                    try {
                        cdt.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else {
                    return ll.pollLast();
                }
            }finally {
                lock.unlock();
            }
            return null;
        }

        public void set(Integer last){
            lock.lock();
            try {
               if (ll.offerFirst(last)) {
                   cdt.signalAll();
               }
            }finally {
                lock.unlock();
            }
        }
    }

}
