package work.labrador.spring.cloud.test.utils;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author 姜陶
 * @date 2022/2/10 17:19
 * @describe 执行队列
 **/
public class ExecuteQueue {

    private static final ReentrantLock LOCK = new ReentrantLock(false);
    private static final Condition CONDITION = LOCK.newCondition();
    private static final LinkedList<Integer> link = new LinkedList<>();

    public ExecuteQueue(){}

    public void addInteger(int length){
        LOCK.lock();
        try {
            for (int i=0;i<length;i++){
                link.offerLast(i);
                CONDITION.signalAll();
            }
        } finally {
            LOCK.unlock();
        }
    }

    public Integer getTask(){
        LOCK.lock();
        try {
            if (link.isEmpty()){
                CONDITION.await();
            }
            return link.pollFirst();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            LOCK.unlock();
        }
        return 100000000;
    }
}
