package work.labrador.spring.cloud.test.utils;

import org.apache.http.util.TextUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 姜陶
 * @date 2022/2/17 8:38
 * @describe BIO执行
 **/
public class BioSocket {
    private static final int PORT = 8080;
    private static final String HOST = "127.0.0.1";
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    public static void main(String[] args) {
        executor.submit(BioSocket::start);
        executor.submit((Runnable) () -> {
            while (true) {
                BIOClient.sendMsg("msg: 1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static synchronized void start() {

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            while (true) {
                System.out.println("服务端启动,可以处理连接了...");
                Socket socket = serverSocket.accept();
                // 启动新线程执行连接任务
                executor.submit(new SocketHandler(socket));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class SocketHandler implements Runnable {
        private Socket socket;

        public SocketHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (BufferedReader r = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 BufferedWriter w = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
                while (true) {
                    String line = r.readLine();
                    if (TextUtils.isEmpty(line)) {
                        System.out.println("执行结束，销毁任务...");
                        return;
                    }
                    if (!TextUtils.isEmpty(line)) {
                        System.out.println("服务器readline: " + line);
                        String result = "线程名称: " + Thread.currentThread() + "消息内容: " + line;
                        w.write(result);
                        w.newLine();
                        w.flush();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    static class BIOClient {
        public static void sendMsg(String msg) {
            try (Socket socket = new Socket(HOST, PORT);
                 BufferedReader r = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 BufferedWriter w = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))) {
                System.out.println(socket.getTcpNoDelay());
                w.write(msg);
                w.newLine();
                w.flush();
                System.out.println("在线检测..." + socket.getKeepAlive());
                String readMsg = r.readLine();
                if (!TextUtils.isEmpty(readMsg)) {
                    System.out.println("客户端: " + readMsg);
                }
                socket.shutdownOutput();
                socket.shutdownInput();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
